# Documentation for Simple API
This is test API project. It shows entries from ready models from DB.
For example i get Pages module and this table.

For now API have not versioning. API work as individual Yii application. And we can cut this to another server, but you need to use common modules.

**Response format:**
You can change response format (json/xml) by use headers or GET paremetr:
* Header: `Accept: application/json` or `Accept: application/xml`
* GET parametr: `_format=xml` or `_format=json`

Default is json format.


### Login (Authentication)

Make request (`GET`/`POST`) to `/api/auth` with parametrs: "username" and "password".
Example:
```
http://learn-yii.loc/api/auth/?username=root&password=root
```

Test username: "root", test password: "root"

**Response:**  
If login/password are correct, return a token like this `KgDPTMewVV6FbwbIyAybvhKt5aSA9Qv6`. The token will expire on the next day. After that, you need login again.



### API test
Here is simple test for detection of working app/server.  
API request:
```
http://learn-yii.loc/api/site/index
```
Expected response: `api`.


### Module "Pages"
**About module:** This module contain information (URL, title, descriptions, html text etc) about static pages in project.

For API connection to this module you need to be logged in. The API support the next ways of auth:  
1. Barear
2. Classic via token in GET parameter.  
Example: `http://learn-yii.loc/api/pages/ping/?token=KgDPTMewVV6FbwbIyAybvhKt5aSA9Qv6`


#### Method "pages" = "pages/index"
Request:
```
Authorization: Bearer KgDPTMewVV6FbwbIyAybvhKt5aSA9Qv6
Accept: application/json

http://learn-yii.loc/api/pages
```

Response:
```
[
    {
        "id": 1,
        "alias": "/////test",
        "title": "заголовок",
        "page_title": "Тестовая страница",
        "meta_description": "",
        "meta_keywords": "",
        "content": "некий контент"
    },
    {
        "id": 2,
        "alias": "test2",
        "title": "test2",
        "page_title": "test2",
        "meta_description": "test2",
        "meta_keywords": "test2",
        "content": "test2test2test2"
    }
]
```

For addition information you need to add get paremetr `expand` and name of adition field as value.

Method support the next addition fields:
* own_description
* visible - show/hide page
* layout 

