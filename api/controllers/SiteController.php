<?php
namespace api\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return 'api v1.1';
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $model = new \api\models\LoginForm();
        $model->load(array_merge(Yii::$app->request->queryParams, Yii::$app->request->bodyParams), ''); // захватываем данные отовсюду
        if ($token = $model->auth()) {
            return $token;
        } else {
            return $model;
        }
    }


    /**
     * Захватываем ошибки
     *
     * @return array
     */
    public function actionError(){
        $exception = Yii::$app->errorHandler->exception;

        return [
            'message' => $exception->getMessage(),
            'code' => Yii::$app->request->headers,
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
        ];

    }

}
