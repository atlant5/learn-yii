<?php
namespace api\controllers;

use modules\pages\models\api\Pages;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use api\models\LoginForm;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;


/**
 * Pages controller
 * Выдает информацию о статических старницах
 * если запросить доп инфо - то еще и внутреннее примечание
 */
class PagesController extends ActiveController
{
    public $modelClass = 'modules\pages\models\api\Pages';

    public function behaviors() {

        $behaviors = parent::behaviors();


        $behaviors['authenticator']  = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                [
                    'class' => QueryParamAuth::className(),
                    'tokenParam' => 'token',
                ],
                HttpBearerAuth::className(),
            ],
            'except' => ['ping'] // если экшн пинг исключить, то видная разница, что на пинг 403, на все остальное 401. надо фиксить
        ];
        $behaviors['authenticator']['only'][] = 'index';


        // разрешаем доступ к информации о странице авторизованным юзерам
        // ибо нечего неавторизованным с api пытаться выкачать информацию в удобном виде
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index'],
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];


        //VarDumper::dump($this, 10);
        //exit();
        return $behaviors;
    }



    public function verbs() {
        return [
            'index' => ['post', 'get'],
            'ping' => ['post', 'get']
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->findPages();
    }

    private function findPages(){
        return Pages::find()->all();
    }

    public function actionPing() {
        return 'PagesController';
    }



}