<?php

use yii\db\Migration;

/**
 * Handles the creation of table `token`.
 * Миграция на создание таблицы с токенами. Делаем связь с таблицей юзеров.
 */
class m170404_064405_create_token_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('token', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'token' => $this->string()->notNull()->unique(),
            'expired_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-token-user_id', 'token', 'user_id');
        $this->addForeignKey('fk-token_user_id', 'token', 'user_id', 'users', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('token');
    }
}
