<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 16.01.17
 * Time: 0:04
 */

namespace backend\widgets;


class Menu extends \yii\widgets\Menu
{
    public $submenuTemplate = "<ul class='treeview-menu'>{items}</ul>";
    public $encodeLabels = false;

}