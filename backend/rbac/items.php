<?php
return [
    'super-admin' => [
        'type' => 1,
        'description' => 'Роль супер админа. Может всёh',
        'children' => [
            'blog-read',
        ],
    ],
    'blog-crud' => [
        'type' => 2,
        'description' => 'CRUD простого блога',
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Администратор без системных полномочий',
        'children' => [
            'blog-read',
        ],
    ],
    'blog-read' => [
        'type' => 2,
        'description' => 'Читатель блога',
    ],
];
