<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => [
        'log',
        'modules\rbac\Bootstrap',
        'modules\main\Bootstrap', // все бутстрап классы из модулей подключаем сюда

        'modules\general_pages\Bootstrap',
        'modules\backendinfo\Bootstrap',
        'modules\pages\Bootstrap',
        'modules\talk_bot\Bootstrap',
        'modules\simple_blog\Bootstrap',
        'modules\trips\Bootstrap',
    ],
    'modules' => [
        'users' => [
            'class' => 'modules\users\Module',
        ],
        'rbac' => [
            'class' => 'modules\rbac\Module',
        ],
        'main' => [
            'class' => 'modules\main\Module',
        ],
        'backendinfo' => [
            'class' => 'modules\backendinfo\Module',
        ],
        'pages' => [
            'class' => 'modules\pages\Module',
        ],
        'general_pages' => [
            'class' => 'modules\general_pages\Module',
        ],
        'talk_bot' => [
            'class' => 'modules\talk_bot\Module',
        ],
        'simple_blog' => [
            'class' => 'modules\simple_blog\Module',
        ],
        'trips' => [
            'class' => 'modules\trips\Module',
        ],
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            // файлы создаем руками в удобной папке.
            // есть альтернативный вариант - хранение в БД - поменять пару строк, но будет лишний запрос
            // нужно понять как лучше, пока на файлах =)
            'itemFile' => '@backend/rbac/items.php', // хранятся роли и права доступа
            'assignmentFile' => '@backend/rbac/assignments.php', // связь пользоватея с ролью
            'ruleFile' => '@backend/rbac/rules.php', // хранятся правила, особо не используется
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/backend'
        ],
        'user' => [
            'identityClass' => 'modules\users\models\backend\Users', // тут указываем нашу модель для работы с юзерами, а не стандартную
            'enableAutoLogin' => true,
            'loginUrl' => ['/users/auth/login' ],
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'main/default/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'login' => 'users/auth/login',
                'logout' => 'users/auth/logout',
            ],
        ],

    ],
    'as access' => [
        'class' => 'yii\filters\AccessControl',
        'rules' => [
            [
                'allow'       => true,
                'actions'     => ['login', 'registration'],
                'controllers' => ['users/auth'],
                'roles'       => ['?'],
            ],
            [
                'allow'       => false,
                'actions'     => ['login', 'registration'],
                'controllers' => ['users/auth'],
                'roles'       => ['@'],
            ],
            [
                'allow'       => true,
                'roles'       => ['@'],
            ],
            [
                'allow'       => true,
                'actions'     => ['error'],
                'controllers' => ['main/default'],
            ],
        ],
    ],
    'params' => $params,
];
