<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 25.02.17
 * Time: 12:50
 */

namespace common\components\HandpickedListWidget;
use yii\base\Widget;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;

use Closure;
use yii\helpers\ArrayHelper;
use yii\i18n\Formatter;
use yii\base\InvalidConfigException;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\widgets\BaseListView;
use yii\base\Model;

class HandpickedListWidget extends Widget
{
    /**
     * Определение заголовка виджета
     * @var string
     */
    public $title = 'Handpicked list';

    /**
     * CSS classes for general block
     * @var string
     */
    public $class = 'box-warning box-solid';

    /**
     * allow/disallow collapse button
     * @var bool
     */
    public $collapse = true;

    /**
     * Readonly mode. Disable pop-up with all elements. Show only base box with selected items.
     * @var bool
     */
    public $readOnly = false;

    /**
     * Little information if you want.
     * @var null|string
     */
    public $hint = null;
    /**
     * сюда передаем массив всех элементов
     * @var array
     */
    public $dataProvider = [
        0 => [
            'code' => 'TEST',
            'code2' => 'TEST',
            'code3' => 'TEST',
            'name' => 'Первый блок',
            'type' => 'text',
            'checked' => true,
        ],
        1 => [
            'code' => 'TEST2',
            'name' => 'Второй блок',
            'type' => 'text',
            'checked' => true,
        ],
        2 => [
            'code' => 'TEST3',
            'name' => 'test3',
            'type' => 'text',
            'checked' => false,
        ],
        3 => [
            'name' => 'четвертый блок без лейблов',
            'checked' => true,
        ],
        [
            'name' => 'следующий блок',
            'type' => 'text',
            'checked' => false,
        ],
        [
            'name' => 'еще блок',
            'type' => 'text',
            'checked' => true,
        ],
        [
            'name' => 'какой-то блок',
            'type' => 'text',
            'checked' => false,
        ],

    ];

    // еще не работает
    public $checkedColumn = ['active' => true];

    public $bgLabels = ['bg-aqua', 'bg-blue', '']; //бэкграунды лейблов, меняются по очереди

    //public $usingColumns = ['name', 'code', 'type']; //колонки которые использовать при отображении

    //public $detectedColumns = ['code' => 'code', 'widget_type' => 'type']; //колонки по которым вычисляем отмеченные элементы

    public $dataProvider2 = [];


    public function init(){
        parent::init();
    }




    public function selectedItems(){
    $selectedItems = [];

    //VarDumper::dump($this->dataProvider, 10, true);
    //exit();

    foreach ($this->dataProvider as $item){
        if (!isset($item['checked'])) { continue; }
        if ($item['checked'] == true) {
            $selectedItems[] = $item;
        }
    }
    return $selectedItems;
}

    public function run(){
        HandpickedListWidgetAssets::register($this->getView());
        $dataProvider = new ArrayDataProvider([
            'allModels' => $this->dataProvider,
            'pagination' => false,
        ]);

        return $this->render('default', [
            'title' => $this->title,
            'class' => $this->class,
            'collapse' => $this->collapse,
            'readOnly' => $this->readOnly,
            'hint' => $this->hint,
            'dataProvider' => $dataProvider,
            'selectedItems' => $this->selectedItems(),
            'bgLabels' => $this->bgLabels,
            'widgetID' => $this->id,
        ]);
    }
}