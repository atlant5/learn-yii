<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 11.03.17
 * Time: 13:43
 */

namespace common\components\HandpickedListWidget;
use yii\web\AssetBundle;


class HandpickedListWidgetAssets extends AssetBundle
{
    //public $basePath = '@common/components/HandpickedListWidget/assets';
    public $sourcePath = '@common/components/HandpickedListWidget/assets';
    //public $baseUrl = '@web';
    public $css = [];
    public $js = ['js/jquery.filtertable.js'];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
