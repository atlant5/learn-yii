<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170103_190326_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'email' => $this->string(50),
            'password' => $this->string(255),
            'auth_key' => $this->string(255),
            'updated_at' => $this->timestamp(),
            /*'created_at' => 'timestamp DEFAULT NOW()',*/
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
