<?php

namespace modules\users\models\frontend;

use Yii;
use modules\users\models\BaseUsers;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $updated_at
 */
class Users extends BaseUsers
{
//

}
