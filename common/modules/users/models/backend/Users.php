<?php

namespace modules\users\models\backend;

use Yii;
use modules\users\models\BaseUsers;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $updated_at
 */
class Users extends BaseUsers implements IdentityInterface
{
//

}
