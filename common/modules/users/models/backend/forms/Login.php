<?php namespace modules\users\models\backend\forms;

use modules\users\models\backend\Users;
use yii\base\Model;
use Yii;
use yii\helpers\VarDumper;

/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 03.01.17
 * Time: 23:13
 *
 */

class Login extends Model
{
    public $email;
    public $password;
    protected $user;

    public function rules()
    {
        return [
            [['email','password'], 'required'],
            [['email'], 'email'],
            ['email', 'string', 'max' => 50],
            ['password', 'string', 'max' => 255, 'min' => 4 ],
            ['email', 'validateUserAuth'],
        ];
    }

    public function validateUserAuth($attribute, $params = []){
        //
        $model = Users::findOne(['email' => $this->email] );
        $this->user = $model; // присваиваем для дальнейшего использования

        if ($model === null){
            //добавляем ошику что в базе не нашли юзера
            $this->addError('email', 'Такого пользователя нет');
        } elseif (Yii::$app->security->validatePassword($this->password, $model->password) === false) {
            $this->addError('password', 'Пароль неверен');
        }
    }

    //  сама авторизация, назначение сессии, куки и тд
    public function login(){

        return $this->validate() AND Yii::$app->user->login($this->user, time()+86400);
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'password' => 'Пароль',
        ];
    }

    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
            $this->render('error', $error);
    }

}