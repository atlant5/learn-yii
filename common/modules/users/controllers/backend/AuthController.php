<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 03.01.17
 * Time: 21:44
 */
namespace modules\users\controllers\backend;

use modules\users\models\backend\Users;
use yii\web\Controller;
use modules\users\models\backend\forms\Login;
use yii;


class AuthController extends Controller
{


    public function behaviors()
    {
        return [
            [
                'class' => yii\filters\AccessControl::className(),
                'only' => ['login', 'logout'],
                'rules' => [
                    [
                        'actions' =>  ['login'],
                        'allow' => true,
                        'roles' => [ '?' ]
                    ],
                    [
                        'actions' =>  ['logout'],
                        'allow' => true,
                        'roles' => [ '@' ]
                    ],
                ]
            ],
        ];
    }

    public function actionLogin()
    {
        // генерируем хеш пароля для первого юзера
        //return Yii::$app->security->generatePasswordHash('root');
        $this->layout = '/main-auth';

        Yii::$app->layout = Yii::$app->user->isGuest ?
            $this->layout = '/main-auth' :      // or just use 'GuestUser' and
            $this->layout = '/main';    // 'RegisteredUser' since the path
                                        // and the extension are used by
                                        // default


        $model = new Login();

        // вытягиваем (засасываем) данные из пост запроса и передаем их в rules методом load() - примерно так
        if ( $model->load(Yii::$app->request->post())  AND $model->login() ){

            return 'ok';
        }

        return $this->render('login', ['model' => $model]);
    }


    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['login']);
    }



    public function actionFault()
    {
        $exception = Yii::$app->errorHandler->exception;

        if ($exception !== null) {
            $statusCode = $exception->statusCode;
            $name = $exception->getName();
            $message = $exception->getMessage();

            $this->layout = '/main-auth';

            return $this->render('custom-error-view', [
                'exception' => $exception,
                'statusCode' => $statusCode,
                'name' => $name,
                'message' => $message
            ]);
        }
    }



    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'view' => '@modules/users/views/backend/default/index.php '
            ],
        ];
    }


}