<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 01.01.17
 * Time: 19:27
 */

namespace modules\users\controllers\backend;

use yii\web\Controller;


class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }


}