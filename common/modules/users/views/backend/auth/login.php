<?php
/* @var $model \modules\users\models\backend\Login */
// todo вывести имя в главном лейауте админки, кнопку-ссылку выход
// todo сделать страницы ошибок раздельные для юзеров и админов разные

use yii\widgets\ActiveForm;
use yii\helpers\Html;


?>

<div class="login-box-body">
    <p class="login-box-msg">
        <?= Yii::$app->user->isGuest ? 'Вход в панель управления сайтом' : 'Привет, '.Yii::$app->user->identity->name; ?></p>

    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <!--<div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Пароль">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>-->
        <div class="row">
            <!--<div class="col-xs-8">

            </div>

            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div>-->


            <?= $form->field($model, 'email')->input('email', ['autofocus' => true]) ?>


            <?= $form->field($model, 'password')->passwordInput() ?>


            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary']) ?>

            </div>
        </div>




    <?php ActiveForm::end(); ?>

    <hr>
    <div class="row">
        <div class="col-xs-6">
            <a href="#">Запамятовал пароль</a>
        </div>
        <!-- /.col -->
        <div class="col-xs-6 text-right">
            <a href="register.html" class="text-center">Новый пользователь</a>
        </div>
        <!-- /.col -->
    </div>


</div>