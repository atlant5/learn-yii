<?php
/**
 * Базовый класс вспомогательной информации о поездках
 *
 */

namespace modules\trips\models;
use yii\db\ActiveRecord;

class BaseTrips extends ActiveRecord
{

    /**
     * База данных + таблица
     * @return string
     */
    public static function tableName()
    {
        return 'cbt.trip';
    }

    public function getTripsServices()
    {
        return $this->hasMany(BaseTripsServices::className(), ['trip_id' => 'id']);
    }

    public function getFlights()
    {
        return $this->hasMany(BaseFlightSegment::className(), ['flight_id' => 'id'])->via('tripsServices');
    }

    public function getAirports()
    {
        return $this->hasMany(BaseAirportsLib::className(), ['airport_id' => 'depAirportId'])->via('flights');
    }
}