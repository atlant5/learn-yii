<?php
/**
 * Базовый класс вспомогательной информации о поездке:
 * информация о перелетах
 *
 */

namespace modules\trips\models;
use yii\db\ActiveRecord;

class BaseFlightSegment extends ActiveRecord
{

    /**
     * База данных + таблица
     * @return string
     */
    public static function tableName()
    {
        return 'cbt.flight_segment';
    }

}