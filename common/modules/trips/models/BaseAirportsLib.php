<?php
namespace modules\trips\models;
use yii\db\ActiveRecord;

/**
 * Class BaseAirportsLib
 * @package modules\trips\models
 *
 * Базовый класс библиотеки аэропортов.
 *
 * Бибилиотека хранит статические (редкоизменяемые) данные о названиях аэропортов.
 * Находится в отдельной базе&
 */
class BaseAirportsLib extends ActiveRecord
{

    /**
     * База данных + таблица
     * @return string
     */
    public static function tableName()
    {
        return 'nemo_guide_etalon.airport_name';
    }

}