<?php
/**
 * Базовый класс основной информации о поездке
 *
 */

namespace modules\trips\models;
use yii\db\ActiveRecord;

class BaseTripsServices extends ActiveRecord
{

    /**
     * База данных + таблица
     * @return string
     */
    public static function tableName()
    {
        return 'cbt.trip_service';
    }

    public function getFlights()
    {
        return $this->hasMany(BaseFlightSegment::className(), ['flight_id' => 'id']);
    }
}