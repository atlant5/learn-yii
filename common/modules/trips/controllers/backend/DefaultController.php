<?php

namespace modules\trips\controllers\backend;

use modules\trips\models\BaseTrips;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use Yii;

/**
 * DefaultController для Trips модели
 */
class DefaultController extends Controller
{

    /**
     * Lists all Trips.
     * @return mixed
     */
    public function actionIndex()
    {

        $trips = BaseTrips::find()
            ->where(['corporate_id' => 3])
            ->groupBy('id') // todo magic - с пагинацией и без нее показывает разное количество элементов. GroupBy спасает, но надо понять в чем магия.
            ->joinWith('tripsServices')
            ->andWhere(['service_id' => 2]);


        $search = '';
        if (isset(Yii::$app->request->queryParams['search']) AND Yii::$app->request->queryParams['search'] != '') {
            $search = Yii::$app->request->queryParams['search'];
            $trips->joinWith('airports') // джоины срабатывают исключительно при поиске, что бы базу не гонять лишний раз
                ->joinWith('flights')
                ->andWhere(['airport_name.value' => $search]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $trips
        ]);

        return $this->render('index', [
            'search' => $search, // что ввели в поиск
            'dataProvider' => $dataProvider, // то что нашли
        ]);
    }

}
