<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 12.01.17
 * Time: 1:59
 *
 * Бутстрап юзаем для того, что бы заданная таблица роутов подтягивалась сразу.
 * Бутстрап вызывается во время инициализации всего приложения, а не только когда понадобился это модуль
 */

namespace modules\trips;


use yii\base\BootstrapInterface;
use Yii;

class Bootstrap implements BootstrapInterface {


    public function bootstrap($app) {
        $rules = [
            'trips' => 'trips/default/index',
        ];
        $app->urlManager->addRules($rules, false); // false - добавляет эти правила к существующим в конфиге

        // добавляем информацию в главный конфиг при загрузке, чтоб потом вытащить во вьюхе
        // ужасный метод
        Yii::$app->params['leftMenu'][1103] = [
            'label' => '<i class="fa fa-plane"></i> <span>Поездки</span>',
            'url' => ['/trips/default/index/']
        ];


    }
}