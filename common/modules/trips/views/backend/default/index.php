<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel modules\pages\models\backend\SearchPages */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Поиск поездок';
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageTitle'] = $this->title;

?>
<div class="pages-index simple-blog-index  box box-header with-border">

    <?php Pjax::begin(); ?>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <div class="row">
        <div class="col-md-2">
            <input type="text" name="search" class="form-control" value="<?= $search ?>" placeholder="точное соответствие">
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <?= Html::submitButton('найти', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>




    <?php ActiveForm::end(); ?>





    <?= GridView::widget([
        'dataProvider' => $dataProvider,

        /*'columns' => [

            [
                'attribute' => 'id',
                'label' => 'id поездки',
                'filter' => false,
            ],
            'corporate_id',
            'number',
            'user_id',
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
                'value' => function($model){
                    return $model->created_at > 0 ? $model->created_at : null   ;
                }
            ],
            [
                'attribute' => 'updated_at',
                'format' => 'datetime',
                'value' => function($model){
                    return $model->updated_at > 0 ? $model->updated_at : null   ;
                }
            ],
            [
                'attribute' => 'coordination_at',
                'format' => 'datetime',
                'value' => function($model){
                    return $model->coordination_at > 0 ? $model->coordination_at : null   ;
                }
            ],
            [
                'attribute' => 'coordination_at',
                'format' => 'datetime',
                'value' => function($model){
                    return $model->saved_at > 0 ? $model->saved_at : null   ;
                }
            ],
            'tag_le_id',
            'trip_purpose_id',
            'trip_purpose_parent_id',
            'trip_purpose_desc',
            'status',

        ],*/
    ]); ?>
    <?php Pjax::end(); ?>
</div>
