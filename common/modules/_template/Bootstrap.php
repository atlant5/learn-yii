<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 12.01.17
 * Time: 1:59
 *
 * Бутстрап юзаем для того, что бы заданная таблица роутов подтягивалась сразу.
 * Бутстрап вызывается во время инициализации всего приложения, а не только когда понадобился это модуль
 */

namespace modules\_template;


use yii\base\BootstrapInterface;
use Yii;

class Bootstrap implements BootstrapInterface {


    public function bootstrap($app) {
        $rules = [
            '_template' => '_template/default/index',
        ];
        $app->urlManager->addRules($rules, false); // false - добавляет эти правила к существующим в конфиге

        // добавляем информацию в главный конфиг при загрузке, чтоб потом вытащить во вьюхе
        // ужасный метод
        Yii::$app->params['leftMenu'][100] = [
            'label' => '<i class="fa fa-gear"></i> <span>О _template</span>',
            'url' => '#',

            'items' => [
                [
                    'label' => '<i class="fa fa-home"></i> <span>Общая информация</span>',
                    'url' => ['/_template/default/index/'],
                ],
                [
                    'label' => '<i class="fa fa-gear"></i> <span>PHP info</span>',
                    'url' => ['/_template/default/phpinfo'],
                ],
            ]
        ];
    }
}