<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 20.01.17
 * Time: 0:52
 */

namespace modules\pages\components;


use modules\pages\models\frontend\Pages;
use yii\helpers\VarDumper;

// потом этот класс подключается в бутстрапе модуля
class UrlRule extends \yii\web\UrlRule
{

    public $pattern = '';
    public $route ='/pages/default/view';

    /**
     * @inheritdoc
     * этот метод парсит урл и проверяет наличие в базе
     * если есть, то командует на открытие модуля pages
     * убирает слеш в конце
     */
    public function parseRequest($manager, $request){
        $pathInfo = preg_replace('#^(.*?)/$#', '$1', $request->pathInfo);
        $model = Pages::find()->where(['alias' => $pathInfo, 'visible' => 1])->exists();
        if ($model == true) {
            return [
                $this->route,
                ['id' => $pathInfo],
            ];
        } else {
            return false;
        }

        //VarDumper::dump($request, 10, true);
        // exit();
    }
}