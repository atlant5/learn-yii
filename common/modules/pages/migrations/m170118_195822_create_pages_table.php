<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pages`.
 */
class m170118_195822_create_pages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(255),
            'title'  => $this->string(255),
            'page_title' => $this->string(255),
            'own_description' => $this->string(1000),
            'meta_description' => $this->string(255),
            'meta_keywords' => $this->string(255),
            'visible' => $this->boolean(),
            'content' => 'MEDIUMTEXT', //
            'layout' => $this->string(255),
            'created_at' => $this->dateTime(), // 0000-00-00 00:00:00
            'updated_at' => $this->timestamp(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pages');
    }
}
