<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 18.01.17
 * Time: 23:55
 */

namespace modules\pages\models\api;
use modules\pages\models\BasePages;

class Pages extends BasePages
{
    public function fields()
    {
        return [
            'id',
            'alias',
            'title',
            'page_title',
            'meta_description',
            'meta_keywords',
            'content'
        ];
    }

    public function extraFields()
    {
        return [
            'own_description',
            'visible',
            'layout',
        ];
    }
}