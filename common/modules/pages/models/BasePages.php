<?php

namespace modules\pages\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string $alias
 * @property string $title
 * @property string $page_title
 * @property string $own_description
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $visible
 * @property string $content
 * @property string $layout
 * @property string $created_at
 * @property string $updated_at
 */
class BasePages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['visible'], 'integer'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['alias', 'title', 'page_title', 'meta_description', 'meta_keywords', 'layout'], 'string', 'max' => 255],
            [['own_description'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'URL',
            'title' => 'Заголовок < title >',
            'page_title' => 'Название страницы',
            'own_description' => 'Внутренее описание',
            'meta_description' => 'SEO description',
            'meta_keywords' => 'SEO keywords',
            'visible' => 'Опубликовано',
            'content' => 'Содержимое',
            'layout' => 'Layout',
            'created_at' => 'создано',
            'updated_at' => 'изменено',
        ];
    }
}
