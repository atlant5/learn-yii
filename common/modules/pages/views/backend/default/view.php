<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model modules\pages\models\backend\Pages */

$this->title = $model->page_title;
$this->params['breadcrumbs'][] = ['label' => 'Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageTitle'] = 'Страница: '.$this->title

?>
<div class="pages-view">



    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'alias',
            'title',
            'page_title',
            'own_description',
            'meta_description',
            'meta_keywords',
            'visible',
            'content:ntext',
            'layout',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
