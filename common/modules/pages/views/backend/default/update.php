<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\pages\models\backend\Pages */

$this->title = 'Изменение страницы: ' . $model->page_title;
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->page_title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';

$this->params['pageTitle'] = $this->title;
?>
<div class="pages-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
