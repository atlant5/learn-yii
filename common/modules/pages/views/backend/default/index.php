<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel modules\pages\models\backend\SearchPages */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Простые статические страницы';
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageTitle'] = $this->title;

?>
<div class="pages-index">

    <?php Pjax::begin(); ?>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Новая страница', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'id',
                'filter' => false,
            ],
            'page_title',
            'alias',

            'own_description',
            // 'meta_description',
            // 'meta_keywords',
            [
                'attribute' => 'visible',
                'format' => 'html',
                'filter' => [
                    0 => 'Не опубликовано',
                    1 => 'Опубликовано',
                ],
                'options' => [
                    'style' => 'width: 170px;',
                ],
                'value' => function($model) { //вызывается функция на каждой строке
                    /* @var $model \modules\pages\models\backend\Pages */
                    if ($model->visible){
                        return '<div class="label label-block label-success"><i class="fa fa-check"></i></div>';
                    }
                    return '<div class="label label-block label-danger"><i class="fa fa-stop"></i></div>';
                }
            ],
            // 'content:ntext',
            // 'layout',
            // 'created_at',
            // 'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
