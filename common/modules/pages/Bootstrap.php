<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 12.01.17
 * Time: 1:59
 *
 * Бутстрап юзаем для того, что бы заданная таблица роутов подтягивалась сразу.
 * Бутстрап вызывается во время инициализации всего приложения, а не только когда понадобился это модуль
 */

namespace modules\pages;


use modules\pages\components\UrlRule;
use yii\base\BootstrapInterface;
use Yii;

class Bootstrap implements BootstrapInterface {


    public function bootstrap($app) {
        $rules = [
            'pages' => 'pages/default/index',
        ];

        if (Yii::$app->id == 'app-frontend') {
            $rules[] = [
                'class' => UrlRule::className(),
            ];
        }
        $app->urlManager->addRules($rules, false); // false - добавляет эти правила к существующим в конфиге

        // добавляем информацию в главный конфиг при загрузке, чтоб потом вытащить во вьюхе
        // ужасный метод
        Yii::$app->params['leftMenu'][303]['items'][5] =[
            'label' => '<i class="fa fa-file-o"></i> <span>Статические страницы</span>',
            'url' => ['/pages/default/index/'],

        ];
    }
}