<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Боты';
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageTitle'] = $this->title;

?>
<div class="bots-index  box box-header with-border">


    <p>
        <?= Html::a('создать бота', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            'code',
            'gender',
            'status',
            // 'root',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
