<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model modules\talk_bot\models\backend\Bots */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Боты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageTitle'] = $this->title;
?>
<div class="bots-view ">

    <div class="row">
        <div class="col-md-6">
            <script>
                function getAnswer() {
                    $.ajax({
                        url: '<?php echo Yii::$app->request->baseUrl. '/talk_bot/chat/listen' ?>',
                        type: 'post',
                        data: {
                            text: $("#text").val() ,
                            user:$("#user").val() ,
                            _csrf : '<?=Yii::$app->request->getCsrfToken()?>'
                        },
                        success: function (data) {
                            console.log(data);
                        }
                    }).done(function( result )
                    {
                        $("#msg").html( result );
                    });
                }
            </script>


            <div class="box box-success direct-chat direct-chat-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Чат с ботом</h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!-- Conversations are loaded here -->
                    <div class="direct-chat-messages">
                        <!-- Message. Default to the left -->
                        <div class="direct-chat-msg">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-left">root</span>
                                <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                            </div>
                            <!-- /.direct-chat-info -->

                            <div class="direct-chat-text">
                                Is this template really for free? That's unbelievable!
                            </div>
                            <!-- /.direct-chat-text -->
                        </div>
                        <!-- /.direct-chat-msg -->

                        <!-- Message to the right -->
                        <div class="direct-chat-msg right">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-right">Первый бот</span>
                                <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                            </div>
                            <!-- /.direct-chat-info -->

                            <div class="direct-chat-text">
                                You better believe it!
                            </div>
                            <!-- /.direct-chat-text -->
                        </div>
                        <!-- /.direct-chat-msg -->
                    </div>
                    <!--/.direct-chat-messages-->

                    <!-- Contacts are loaded here -->
                    <div class="direct-chat-contacts">
                        <ul class="contacts-list">
                            <li>
                                <a href="#">
                                    <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="User Image">

                                    <div class="contacts-list-info">
                            <span class="contacts-list-name">
                              Count Dracula
                              <small class="contacts-list-date pull-right">2/28/2015</small>
                            </span>
                                        <span class="contacts-list-msg">How have you been? I was...</span>
                                    </div>
                                    <!-- /.contacts-list-info -->
                                </a>
                            </li>
                            <!-- End Contact Item -->
                        </ul>
                        <!-- /.contatcts-list -->
                    </div>
                    <!-- /.direct-chat-pane -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="input-group">
                        <input type="text" name="message" placeholder="введи сообщение ..." class="form-control">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-success btn-flat" onClick="getAnswer()">Отправить</button>
                            </span>
                        <div id="msg"></div>
                    </div>
                </div>
                <!-- /.box-footer-->
            </div>

        </div>
        <div class="col-md-6">
            <div class="box box-header with-border">
                <p>
                    <?= Html::a('изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('удалить', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'name',
                        'code',
                        'description',
                        'gender',
                        'status',
                        'root',
                        'created_at',
                        'updated_at',
                    ],
                ]) ?>
            </div>

        </div>
    </div>


</div>
