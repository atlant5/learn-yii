<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\talk_bot\models\backend\Bots */

$this->title = 'Создание бота';
$this->params['breadcrumbs'][] = ['label' => 'Боты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$this->params['pageTitle'] = $this->title;
?>
<div class="bots-create  box box-header with-border">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
