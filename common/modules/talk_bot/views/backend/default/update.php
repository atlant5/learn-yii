<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\talk_bot\models\backend\Bots */

$this->title = 'изменяем бота: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'боты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'изменить';


$this->params['pageTitle'] = $this->title;
?>
<div class="bots-update  box box-header with-border">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
