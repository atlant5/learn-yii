<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 12.01.17
 * Time: 1:59
 *
 * Бутстрап юзаем для того, что бы заданная таблица роутов подтягивалась сразу.
 * Бутстрап вызывается во время инициализации всего приложения, а не только когда понадобился это модуль
 */

namespace modules\talk_bot;


use yii\base\BootstrapInterface;
use Yii;

class Bootstrap implements BootstrapInterface {


    public function bootstrap($app) {
        $rules = [
            'talk_bot' => 'talk_bot/default/index',
        ];
        $app->urlManager->addRules($rules, false); // false - добавляет эти правила к существующим в конфиге

        // добавляем информацию в главный конфиг при загрузке, чтоб потом вытащить во вьюхе
        // ужасный метод
        Yii::$app->params['leftMenu'][400] = [
            'label' => '<i class="fa fa-comments-o"></i> <span>Разговорный бот</span>',
            'url' => '#',

            'items' => [
                [
                    'label' => '<i class="fa fa-list"></i> <span>Список ботов</span>',
                    'url' => ['/talk_bot/default/index/'],
                ],
                [
                    'label' => '<i class="fa fa-gear"></i> <span>PHP info</span>',
                    'url' => ['/talk_bot/default/phpinfo'],
                ],
            ]
        ];
    }
}

