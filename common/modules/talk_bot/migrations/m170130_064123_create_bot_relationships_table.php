<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bot_relationships`.
 */
class m170130_064123_create_bot_relationships_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        // таблица для хранения информации о взаимоотношениях,
        // этапе переговоров со всеми юзерами. тут будет куча настроек
        $this->createTable('bot_relationships', [
            'id' => $this->primaryKey(),
            'bot_id' => $this->integer(), //
            'user_id' => $this->integer(), //
            'mode' => $this->integer(1), // режим работы: 0 - обычный, 1 - debug


            'created_at' => $this->dateTime(), // 0000-00-00 00:00:00
            'updated_at' => $this->timestamp(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('bot_relationships');
    }
}
