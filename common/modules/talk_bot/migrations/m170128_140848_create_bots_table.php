<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bots`.
 */
class m170128_140848_create_bots_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bots', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100), // человеческое название бота
            'code' => $this->string(60), // код бота, некий идентификатор
            'description' => $this->string(1000), // собственное примечание для внутреннего использования
            'gender' => $this->integer(1), // гендерный тип 1 - мальчик, 2 - девочка, 3 - бесполое существо
            'status' => $this->integer(1), // 0 или пустое - тестовый режим, 1- активный боевой режим, 2 - отключен
            'root' => $this->string(100), // код, открывающий root доступ к боту и его системному управлению


            'created_at' => $this->dateTime(), // 0000-00-00 00:00:00
            'updated_at' => $this->timestamp(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('bots');
    }
}
