<?php

namespace modules\talk_bot\models;

use Yii;

/**
 * This is the model class for table "bots".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $description
 * @property integer $gender
 * @property integer $status
 * @property string $root
 * @property string $created_at
 * @property string $updated_at
 */
class BaseBots extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bots';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gender', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'root'], 'string', 'max' => 100],
            [['code'], 'string', 'max' => 60],
            [['description'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'название',
            'code' => 'код бота',
            'description' => 'описание',
            'gender' => 'пол',
            'status' => 'статус',
            'root' => 'ключ доступа',
            'created_at' => 'создано',
            'updated_at' => 'изменено',
        ];
    }
}
