<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\general_pages\models\backend\HtmlBlockWidget */

$this->title = 'Create Html Block Widget';
$this->params['breadcrumbs'][] = ['label' => 'Html Block Widgets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="html-block-widget-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
