<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model modules\general_pages\models\backend\HtmlBlockWidget */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="html-block-widget-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'disabled' => $model->isNewRecord == false ]) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'own_description')->textarea(['maxlength' => true, 'rows' => 2]) ?>
        </div>
    </div>



    <?=
    $form->field($model, 'content')->widget(
        'trntv\aceeditor\AceEditor',
        [
            'mode'=>'html', // programing language mode. Default "html"
            'theme'=>'tomorrow_night', // editor theme. Default "github"
        ]
    )
    ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
