<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\general_pages\models\backend\HtmlBlockWidget */

$this->title = 'Update Html Block Widget: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Html Block Widgets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="html-block-widget-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
