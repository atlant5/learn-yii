<?php

use yii\helpers\Html;
use yii\grid\GridView;



/* @var $this yii\web\View */
/* @var $model modules\general_pages\models\backend\GeneralPages */

$this->title = 'Изменяем страницу: ' . $model->page_title;
$this->params['breadcrumbs'][] = ['label' => 'Страницы сайта', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->page_title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'изменить';


$this->params['pageTitle'] = $this->title;



?>
<div class="general-pages-update">


    <?= $this->render('_form', [
        'model' => $model,
        'dataProvider' => $dataProvider,
        'dataProvider2' => $dataProvider2,
        'handpickedListProvider' => $handpickedListProvider,
        'selectedItems' => $selectedItems,
    ]) ?>

</div>
