<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use atlant5\handpickedlist\HandpickedList;

use \common\components\HandpickedListWidget\HandpickedListWidget;
/* @var $this yii\web\View */
/* @var $model modules\general_pages\models\backend\GeneralPages */
/* @var $form yii\widgets\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */

// \yii\helpers\VarDumper::dump($dataProvider2, 10, true);
// exit();

?>

<div class="general-pages-form box box-header with-border">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'page_title')->textInput(['maxlength' => true]) ?>

            URL: <?= $model->alias ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'own_description')->textarea(['maxlength' => true]) ?>
        </div>
    </div>

    <br>
    <br>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'title')->textarea(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'meta_description')->textarea(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'meta_keywords')->textarea(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">

        </div>
        <div class="col-md-4">

            <?php  $form->field($model, 'manageWidgets[]')->hiddenInput(['name' => '123'])->label(false) ?>
            <!--<input type="hidden" id="generalpages-managewidgets" class="form-control" name="GeneralPages[manageWidgets][]">-->

            <?= HandpickedListWidget::widget([
                'dataProvider' => $handpickedListProvider,
                'dataProvider2' => $dataProvider2,
                'title' => 'Блоки страницы',
                'readOnly' => true,
                'collapse' => false,
                //'hint' => "Блоки, используемые страницой. Можно нажать и отредактировать их содержимое.",
            ]) ?>

                <?= HandpickedList::widget([]) ?>

        </div>
        <div class="col-md-3">
            <br>
            Создано: <?= $model->created_at ?><br>
            Обновлено: <?= $model->updated_at ?>
        </div>
    </div>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить изменения', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
