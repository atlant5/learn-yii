<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model modules\general_pages\models\backend\GeneralPages */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'General Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="general-pages-view box box-header with-border">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'page_title',
            'own_description',
            'alias',
            'title',
            'meta_description',
            'meta_keywords',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
