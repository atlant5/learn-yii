<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\general_pages\models\backend\GeneralPages */

$this->title = 'Create General Pages';
$this->params['breadcrumbs'][] = ['label' => 'General Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="general-pages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
