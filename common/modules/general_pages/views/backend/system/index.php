<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel modules\general_pages\models\backend\SearchGeneralPages */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройка страниц сайта';
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageTitle'] = $this->title;


?>
<div class="general-pages-index  box box-header with-border">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= Html::a('Добавить новую страницу', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="col-md-8">
            <p>
                Добавляйте и управляйте страницами только в случае их фактического изменения. Это вспомогательный интерфей для разработчика.
            </p>
        </div>
    </div>
    <br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            // если не хотим видеть фильтр
            [
                'attribute' => 'id',
                'filter' => false,
            ],
            'page_title',
            //'own_description',
            'alias',
            //'title',
            // 'meta_description',
            // 'meta_keywords',
            // 'created_at',
            [
                'attribute' => 'updated_at',
                'filter' => false,
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<div class="text-center" style="width: 100px;">{update} {delete}</div>',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
