<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\general_pages\models\backend\GeneralPages */

$this->title = 'Системное изменение: ' . $model->page_title;
$this->params['breadcrumbs'][] = ['label' => 'Настройка страниц', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->page_title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';


$this->params['pageTitle'] = $this->title;
?>

<p>
    Вносите изменения в эти настройки только вслучае изменения страниц сайта.
</p>
<div class="general-pages-update box box-header with-border">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
