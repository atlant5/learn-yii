<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Все виджеты сайта';
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageTitle'] = $this->title;
?>
<div class="widgets-index box box-header with-border">

    <p>
        <?= Html::a('Создать виджет', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            'code',
            'name',
            'own_description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
