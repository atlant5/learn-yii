<?php

use yii\db\Migration;

/**
 * Handles the creation of table `widgets_connect`.
 */
class m170122_222912_create_widgets_connect_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        // таблица для соединения виджетов (текстовых блоков, слайдеров и прочих) со страницами. Многие ко многим
        $this->createTable('widgets_connect', [
            'id' => $this->primaryKey(),
            'page_id' => 'SMALLINT', // ID страницы с которой связывается виджет (может все таки связь по алиасу лучше?)
            'widget_id' => 'SMALLINT', // код виджета с которым осуществляется связь
            'updated_at' => $this->timestamp(),
        ]);
    }
    // {widget.slider.home_slider}                  <--- это для статических страниц
    // {*.*.*}
    // <?= GetSlider::render('home_slider') ? >     <--- это для хардкодных страниц

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('widgets_connect');
    }
}
