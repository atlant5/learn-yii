<?php

use yii\db\Migration;

/**
 * Handles the creation of table `text_blocks`.
 */
class m170122_221407_create_html_blocks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('html_blocks', [
            'id' => $this->primaryKey(),
            'code' => $this->string(60), // Системный код блока, который хардкодится на страницу
            'content' => 'MEDIUMTEXT', //собственно содержимое блока, которое выводится на страницу
            'updated_at' => $this->timestamp(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('html_blocks');
    }
}
