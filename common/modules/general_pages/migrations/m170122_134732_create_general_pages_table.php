<?php

use yii\db\Migration;

/**
 * Handles the creation of table `general_pages`.
 */
class m170122_134732_create_general_pages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('general_pages', [
            'id' => $this->primaryKey(),
            'page_title' => $this->string(255), // Заголовок страницы для внутреннего использования
            'own_description' => $this->string(1000), // собственное примечание для внутреннего использования
            'alias' => $this->string(255), //URL страницы, сделать его динамическим пока хз как
            'title'  => $this->string(255), // <title> страницы
            'meta_description' => $this->string(255), // сео дескрипшн
            'meta_keywords' => $this->string(255), // сео ключевики

            'created_at' => $this->dateTime(), // 0000-00-00 00:00:00
            'updated_at' => $this->timestamp(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('general_pages');
    }
}
