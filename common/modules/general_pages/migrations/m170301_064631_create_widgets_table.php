<?php

use yii\db\Migration;

/**
 * Handles the creation of table `widgets`.
 */
class m170301_064631_create_widgets_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('widgets', [
            'id' => $this->primaryKey(),
            'code' => $this->string(60), // Системный код блока, который хардкодится на страницу
            'name' => $this->string(60), // человеческое название блока
            'own_description' => $this->string(1000), // собственное примечание для внутреннего использования
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('widgets');
    }
}
