<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 12.01.17
 * Time: 1:59
 *
 * Бутстрап юзаем для того, что бы заданная таблица роутов подтягивалась сразу.
 * Бутстрап вызывается во время инициализации всего приложения, а не только когда понадобился это модуль
 */

namespace modules\general_pages;


use yii\base\BootstrapInterface;
use Yii;

class Bootstrap implements BootstrapInterface {


    public function bootstrap($app) {
        $rules = [
            'general_pages' => 'general_pages/default/index',
        ];
        $app->urlManager->addRules($rules, false); // false - добавляет эти правила к существующим в конфиге

        // добавляем информацию в главный конфиг при загрузке, чтоб потом вытащить во вьюхе
        // ужасный метод
        Yii::$app->params['leftMenu'][303] = [
            'label' => '<i class="fa fa-sitemap"></i> <span>Страницы сайта</span>',
            'url' => '#',

            'items' => [
                1 => [
                    'label' => '<i class="fa fa-copy"></i> <span>Список страниц</span>',
                    'url' => ['/general_pages/default/index/'],
                ],
                2 => [
                    'label' => '<i class="fa fa-object-group"></i> <span>HTML блоки</span>',
                    'url' => ['/general_pages/html-block/index/'],
                ],
                98 => [
                    'label' => '<i class="fa fa-gear"></i> <span>Виджеты</span>',
                    'url' => ['/general_pages/widgets/index/'],
                ],
                99 => [
                    'label' => '<i class="fa fa-gear"></i> <span>Настройка страниц</span>',
                    'url' => ['/general_pages/system/index/'],
                ],
            ]
        ];
    }
}