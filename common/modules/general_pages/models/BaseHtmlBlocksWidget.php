<?php

namespace modules\general_pages\models;

use Yii;

/**
 * This is the model class for table "html_blocks".
 *
 * @property integer $id
 * @property string $code
 * @property string $content
 * @property string $updated_at
 * @property BaseWidgets $widget
 */
class BaseHtmlBlocksWidget extends \yii\db\ActiveRecord
{
    public $name;
    public $own_description;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'html_blocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['updated_at'], 'safe'],
            [['name'], 'required'],
            [['name'], 'string', 'min' => 2, 'max' => 60],
            [['own_description'], 'string', 'max' => 1000],
            [['code'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'CODE',
            'content' => 'Содержимое',
            'updated_at' => 'изменено',
            'name' => 'Название',
            'own_description' => 'Примечание',
        ];
    }

    public function getWidget() {
        return $this->hasOne(BaseWidgets::className(), ['code' => 'code']); // делаем связь таблиц по полям
    }

    public function loadWidgetInfo(){
        $widget = $this->widget;
        if ($widget != null) {
            $this->name = $widget->name;
            $this->own_description = $widget->own_description;
        }
    }
}
