<?php

namespace modules\general_pages\models;

use Yii;

/**
 * This is the model class for table "widgets_connect".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $code
 * @property string $widget_type
 * @property string $updated_at
 */
class BaseWidgetsConnect extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'widgets_connect';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id'], 'integer'],
            [['widget_id'], 'integer'],
        ];
    }

    public function getWidget() {
        return $this->hasOne(BaseWidgets::className(), ['id' => 'widget_id']); // делаем связь таблиц по полям
    }

    /**
     * Получаем данные обо всех пристыкованных к странице виджетах.
     * входящий параметр - id интересующей нас страницы
     * @param $id int
     */
    public function loadPageWidgets($id){
        //
    }
    /**
     * @inheritdoc
     */
    /*public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'ID страницы',
            'code' => 'Код виджета',
            'widget_type' => 'Тип виджета',
            'updated_at' => 'обновлено',
        ];
    }*/
}
