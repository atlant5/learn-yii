<?php

namespace modules\general_pages\models;

use Yii;

/**
 * This is the model class for table "general_pages".
 *
 * @property integer $id
 * @property string $page_title
 * @property string $own_description
 * @property string $alias
 * @property string $title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $created_at
 * @property string $updated_at
 */
class BaseGeneralPages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'general_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_title', 'alias', 'title', 'meta_description', 'meta_keywords'], 'string', 'max' => 255],
            [['own_description'], 'string', 'max' => 1000],
            [['page_title', 'alias', 'title'], 'required'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_title' => 'Страница',
            'own_description' => 'Внутреннее примечание',
            'alias' => 'URL',
            'title' => 'Заголовок < title >',
            'meta_description' => 'SEO Description',
            'meta_keywords' => 'SEO Keywords',
            'created_at' => 'создано',
            'updated_at' => 'изменено',
        ];
    }


/*    public function getAllText(){
        return $this->hasMany(TextBlocs::classname(), [
            'page_id' => 'id',
        ] );*/
        // $model->allText <- масив всех текстовых блоков таблицы
        // в контроллере пишу модель GeneralPag::find()->where('условие')->with('allText')->all();
        // и после такой конструкции будет всего 2 запроса (ну или около того, явно не 50)
        //
    //}
}
