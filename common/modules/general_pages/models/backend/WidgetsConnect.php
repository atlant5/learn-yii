<?php

namespace modules\general_pages\models\backend;

use modules\general_pages\models\BaseWidgetsConnect;
use Yii;

/**
 * This is the model class for table "widgets_connect".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $code
 * @property string $widget_type
 * @property string $updated_at
 */
class WidgetsConnect extends BaseWidgetsConnect
{
    //


}
