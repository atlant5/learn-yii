<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 05.03.17
 * Time: 21:46
 */

namespace modules\general_pages\models\backend;


use modules\general_pages\models\BaseHtmlBlocksWidget;
use yii\helpers\VarDumper;

/**
 * Class HtmlBlockWidget
 * @package modules\general_pages\models\backend
 */
class HtmlBlockWidget extends BaseHtmlBlocksWidget
{
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $widget = $this->widget; // получаю виджет из соседней таблицы (table widgets)

        //VarDumper::dump($widget, 10, true);
        //exit();
        if ($widget == null) {
            $widget = new Widgets();
            $widget->code = $this->code;
            $widget->name = $this->name;
            $widget->own_description = $this->own_description;

            $widget->save();
        }
        else {
            $widget->updateAttributes(['name' => $this->name, 'own_description' => $this->own_description ]);
        }
    }

    // todo Сделать одновременное удаление из двух таблиц
    public function beforeDelete()
    {
        $widget = $this->widget;
        if ($widget != null) {
            $widget->delete();
        }
        return parent::beforeDelete();
    }

}
