<?php
/**
 *
 * Created by PhpStorm.
 * User: evgeny
 * Date: 22.01.17
 * Time: 18:44
 */

namespace modules\general_pages\models\backend;
use modules\general_pages\models\BaseGeneralPages;
use modules\general_pages\models\backend\WidgetTextBlocks;
use modules\general_pages\models\backend\Widgets;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * Class GeneralPages
 * @package modules\general_pages\models\backend
 *
 * @property WidgetsConnect[] $widgets
 */
class GeneralPages extends BaseGeneralPages
{
    //
    public $manageWidgets = []; // для космоса это не будет работать ибо сделано сейчас только для бэкенда

    public function rules()
    {
        $rules = [
            [
                ['manageWidgets'],
                'in', 'range' => /*array_values(ArrayHelper::map($this->allWidgets(), 'id', 'id'))*/ [4,5],
                'allowArray' => true,
            ], //просто разрешаем запись в поле. Валидации тут никакой нет
        ];

        return array_merge(parent::rules(), $rules); // объединяем старые правила с новыми
    }


    public function beforeValidate()
    {
        $this->manageWidgets = Yii::$app->request->post('selection');
        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }


    public function attributeLabels()
    {
        return  array_merge(parent::attributeLabels(), [
            'manageWidgets' => 'Виджеты страницы',
        ]);
    }


    public function allWidgets(){
        $tables = [
            'html_blocks'
        ];

        $sql = [];
        foreach ($tables as $table){
            $sql[] = "SELECT * FROM $table "; // говнокод галимый, но зато 1 запрос к базе.
        }


        return Widgets::find()->asArray(true)->all();

    }


    /**
     * Сохраняем кросстабличные изменения, а именно записываем виджеты.
     * Пока они пытаются сохраниться при сохранении других данных страницы,
     * даже если виджеты не изменялись.
     * @param bool $insert
     */
    public function beforeSave($insert)
    {
        $this->saveWidgets();
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * Метод сохраняет новый набор виджетов. Старый удаляет.
     */
    protected function saveWidgets(){
        WidgetsConnect::deleteAll(['page_id' => $this->id]); // удаляет все связи виджетов для этой страницы. Делаем так, что бы не делать лишних проверок
        if (is_array($this->manageWidgets) == false) { $this->manageWidgets = []; }
        foreach ($this->manageWidgets as $widgetID){
            if (empty($widgetID)){  //если code пустой убираем последнюю галку
                continue;
            }

            // todo пока делаем так, потом переделаем на то чтобы в одном запросе было сразу реализована запись сразу всех записей
            $model = new WidgetsConnect();
            $model->page_id = $this->id;
            $model->widget_id = $widgetID;
            $model->save();
        }

    }


    public function getWidgets(){
        // возвращаем все связанные виджеты с этой 'page_id' => 'id' страницей
        // 'page_id' - поле в таблице widget_connect (название таблицы ищит через эту штуку WidgetsConnect::className() )
        // id - поле в таблице текущей модели
        return $this->hasMany(WidgetsConnect::className(), ['page_id' => 'id']);
    }

    /**
     * Возвращает массив ID виджетов, принадлежащих этой странице
     * @return array
     */
    public function loadManageWidgets(){
        // заполняем manageWidgets значениями выбранными из базы (CODE = 'TEST' или 'TEST2' и т.п)
        // чтоб автоматом получать именно нужные code, делаем это через ArrayHelper

        $values = [];
        foreach ($this->widgets as $widget) {
            $values[] = $widget->widget_id;
        }

        return $values;
    }

}