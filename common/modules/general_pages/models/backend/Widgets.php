<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 02.03.17
 * Time: 0:50
 */

namespace modules\general_pages\models\backend;


use modules\general_pages\models\BaseWidgets;
use yii\helpers\VarDumper;

class Widgets extends BaseWidgets
{
    // готовим данные для HandpickedList
    public function cookData($checkedItems = []){
        $values = [];
        $model = new GeneralPages;
        $allWidgets = $model->allWidgets();

        if (is_array($checkedItems) == false) { $checkedItems = []; }

        foreach ($allWidgets as $widget) {
            $values[] = [
                'name' => $widget['name'],
                'code' => $widget['code'],
                'checked' => in_array($widget['id'], $checkedItems), // тут некая функция нужна определяющая отмеченность
                'type' => '', // тут некая функция определяющая тип виджета, скорее пригодится для создания ссылки
                'id' => $widget['id'], // это поле нужно для  того, чтоб сделать ссылку на форму редактирования конкретного виджета
            ];
        }
        //VarDumper::dump($checkedItems, 10, true);
        //$model->manageWidgets;
        //exit();
        return $values;

        //$selectedItems = $model->loadManageWidgets();
        //VarDumper::dump($model->loadManageWidgets(), 10, true);
        //exit();

    }
}
