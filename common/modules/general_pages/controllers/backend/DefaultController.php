<?php

namespace modules\general_pages\controllers\backend;

use modules\general_pages\models\backend\Widgets;
use modules\general_pages\models\backend\WidgetsConnect;
use modules\general_pages\models\backend\WidgetTextBlocks;
use Yii;
use modules\general_pages\models\backend\GeneralPages;
use modules\general_pages\models\backend\SearchGeneralPages;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use yii\db\Query;

/**
 * DefaultController implements the CRUD actions for GeneralPages model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all GeneralPages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchGeneralPages();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GeneralPages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Creates a new GeneralPages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GeneralPages();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing GeneralPages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        // заполняем manageWidgets значениями выбранными из базы (CODE = 'TEST' или 'TEST2' и т.п)
        // чтоб автоматом получать именно нужные code, делаем это через ArrayHelper
        $model->manageWidgets = Yii::$app->request->isPost ? Yii::$app->request->post('selection') : $model->loadManageWidgets();

        $allWidgets = new Widgets();

        $cookData = $allWidgets->cookData( is_array($model->manageWidgets) ? $model->manageWidgets : []);

        $dataProvider2 = $model->getWidgets();
        VarDumper::dump($model->allWidgets(), 10, true);
        exit();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $model->allWidgets(),
        ]);

        $selectedItems = new ArrayDataProvider([
            'allModels' => WidgetsConnect::findAll(['page_id' => $id]),
        ]);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'handpickedListProvider' => $cookData,
                'dataProvider2' => $dataProvider2,
                'selectedItems' => $selectedItems,
            ]);
        }
    }


    // todo вспомогательная функция, для форматирования ввода в виджет. поотом переместить в модель и спросить у народа как сделать лучше
    /**
     * нужно чтоб функция вернула массив, где выбранные элементы имеют определенный признак
     *
     */
    public function handpickedBlocks(){
        return [];
    }



    /**
     * Deletes an existing GeneralPages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GeneralPages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GeneralPages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GeneralPages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
