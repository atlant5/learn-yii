<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 01.01.17
 * Time: 19:27
 */

namespace modules\main\controllers\frontend;

use yii\web\Controller;


class DefaultController extends Controller
{

    public function init()
    {
        parent::init();
        $this->layout = \Yii::$app->user->isGuest ? '/main-auth' : '/main';
    }


    public function actionIndex()
    {
        return $this->render('index');
    }
}