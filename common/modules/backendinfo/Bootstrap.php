<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 12.01.17
 * Time: 1:59
 *
 * Бутстрап юзаем для того, что бы заданная таблица роутов подтягивалась сразу.
 * Бутстрап вызывается во время инициализации всего приложения, а не только когда понадобился это модуль
 */

namespace modules\backendinfo;


use yii\base\BootstrapInterface;
use Yii;

class Bootstrap implements BootstrapInterface {


    public function bootstrap($app) {
        $rules = [
            'backendinfo' => 'backendinfo/default/index',
            'backend-info' => 'backendinfo/default/index',
            'phpinfo' => 'backendinfo/default/phpinfo',
        ];
        $app->urlManager->addRules($rules, false); // false - добавляет эти правила к существующим в конфиге

        // добавляем информацию в главный конфиг при загрузке, чтоб потом вытащить во вьюхе
        // ужасный метод
        Yii::$app->params['leftMenu'][100] = [
            'label' => '<i class="fa fa-gear"></i> <span>О системе</span>',
            'url' => '#',

            'items' => [
                [
                    'label' => '<i class="fa fa-home"></i> <span>Общая информация</span>',
                    'url' => ['/backendinfo/default/index/'],
                ],
                [
                    'label' => '<i class="fa fa-gear"></i> <span>PHP info</span>',
                    'url' => ['/backendinfo/default/phpinfo'],
                ],
            ]
        ];
    }
}