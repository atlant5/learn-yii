<?php
/* @var $this \yii\web\View */

$this->params['pageTitle'] = "Cерверная информация phpinfo";

$this->params['breadcrumbs'][] = 'phpinfo';

?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">О серверных модулях</h3>
    </div>
    <div class="box-body">

        <iframe src="<?= \yii\helpers\Url::to(['/backendinfo/default/phpinfo-frame']) ?>" frameborder="0" width="100%" height="700px">

        </iframe>

    </div>
    <!-- /.box-body -->
</div>