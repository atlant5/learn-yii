<?php
/* @var $this \yii\web\View */

$this->params['pageTitle'] = "Общая информация о системе";
$this->params['breadcrumbs'][] = [
    'label' => ' Общая информация',
    'url' => ['/backendinfo/default/index'],
];
$this->params['breadcrumbs'][] = ' Общая';
?>

<div class="row">
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>PHP <?= phpversion() ?></h3>

                <p>Текущая версия PHP на сервере</p>
            </div>
            <div class="icon">
                <i class="fa fa-gear"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>Base 0.0.2 </h3>

                <p>Текущая версия (релиз) сайта</p>
            </div>
            <div class="icon">
                <i class="fa fa-code-fork"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-4 col-xs-6">
    </div>
</div>

<div class="row">
    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">О системе</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Свернуть">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                Сайт написан на фреймворке Yii2, что позволяет расширять и реализовывать индивидуальные особенности. Даже ваша бабушка или собака сможет написать модуль
            </div>
            <!-- /.box-body -->
        </div>

    </div>
    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Рзработчик</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Свернуть">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                Backend made by <a href="https://vk.com/soft.soft" target="_blank">Evgeny Vetrov</a>.
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

