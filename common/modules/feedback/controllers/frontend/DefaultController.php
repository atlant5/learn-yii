<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 01.01.17
 * Time: 19:27
 */

namespace modules\feedback\controllers\frontend;

use modules\feedback\models\frontend\Feedback;
use yii\web\Controller;
use Yii;

class DefaultController extends Controller
{



    public function actionIndex()
    {
        /* Создаем экземпляр класса */
        $model = new Feedback();

        if ($model->load(Yii::$app->request->post()) && $model->contact()) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
            /* иначе выводим форму обратной связи */
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }

        return $this->render('index');
    }


    public function actionContact()
    {
        /* Создаем экземпляр класса */
        $model = new Feedback();
        /* получаем данные из формы и запускаем функцию отправки contact, если все хорошо, выводим сообщение об удачной отправке сообщения на почту */
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['emailTo'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
            /* иначе выводим форму обратной связи */
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }
}