<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 24.02.17
 * Time: 16:38
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

    <div class="alert alert-success">
        Спасибо за обращение к нам. Мы постараемся ответить вам как можно скорее.
    </div>

<?php endif; ?>

<?php $form = ActiveForm::begin(); ?>


<?= $form->field($model, 'subject') ?>
<?= $form->field($model, 'text')->textArea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Отправить сообщение', ['class' => 'btn btn-success', 'name' => 'contact-button']) ?>
    </div>

<?php ActiveForm::end(); ?>