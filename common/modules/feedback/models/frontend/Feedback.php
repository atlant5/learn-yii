<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 24.02.17
 * Time: 17:00
 */

namespace modules\feedback\models\frontend;


use Yii;
use yii\base\Model;


class Feedback extends Model {
    //
    public $text;
    public $subject = 'no subject';
    public $userInfo;
    public $emailFrom = 'site@atlant5.com';
    public $emailTo = 'post.vetrov@gmail.com';

    /* Правила для полей формы обратной связи (валидация) */
    public function rules()
    {
        return [
            /* Поля обязательные для заполнения */
            [ ['text', 'subject'], 'required'],
        ];
    }


    /* Определяем названия полей */
    public function attributeLabels()
    {
        return [
            'text' => 'Отзыв',
            'subject' => 'Тема'
        ];
    }

    /* функция отправки письма на почту */
    public function contact()
    {
        /* Проверяем форму на валидацию */
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setFrom([$this->emailFrom => $this->emailFrom]) /* от кого */
                ->setTo($this->emailTo) /* куда */
                ->setSubject($this->subject) /* имя отправителя */
                ->setTextBody($this->text) /* текст сообщения */
                ->send(); /* функция отправки письма */

            return true;
        } else {
            return false;
        }
    }

}
