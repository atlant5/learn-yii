<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 12.01.17
 * Time: 1:59
 *
 * Бутстрап юзаем для того, что бы заданная таблица роутов подтягивалась сразу.
 * Бутстрап вызывается во время инициализации всего приложения, а не только когда понадобился это модуль
 */

namespace modules\main;


use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface {

    public function bootstrap($app) {
        $rules = [
            '' => 'main/default/index',
        ];

        $app->urlManager->addRules($rules, false); // false - добавляет эти правила к существующим в конфиге
    }

}