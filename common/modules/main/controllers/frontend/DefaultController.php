<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 01.01.17
 * Time: 19:27
 */

namespace modules\main\controllers\frontend;

use yii\web\Controller;


class DefaultController extends Controller
{


    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ]
        ];
    }
}