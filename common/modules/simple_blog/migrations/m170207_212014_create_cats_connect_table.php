<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cats_connect`.
 */
class m170207_212014_create_cats_connect_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cats_connect', [
            'id' => $this->primaryKey(),
            'post_id' => $this->string(255),
            'category_id' => $this->string(255),
            'updated_at' => $this->timestamp(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cats_connect');
    }
}
