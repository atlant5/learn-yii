<?php

use yii\db\Migration;

/**
 * Handles the creation of table `posts`.
 */
class m170130_221511_create_posts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('posts', [
            'id' => $this->primaryKey(),
            'title'  => $this->string(255),
            'own_description'  => $this->string(800),
            'page_title' => $this->string(255),
            'meta_description'  => $this->string(255),
            'meta_keywords'  => $this->string(255),
            'content'  => 'MEDIUMTEXT',

            'publication' => $this->dateTime(), // дата публикации (может быть и в будущем) 0000-00-00 00:00:00
            'created_at' => $this->dateTime(), // 0000-00-00 00:00:00
            'updated_at' => $this->timestamp(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('posts');
    }
}
