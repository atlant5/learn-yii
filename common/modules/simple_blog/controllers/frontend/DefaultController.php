<?php

namespace modules\simple_blog\controllers\frontend;

use modules\simple_blog\models\frontend\SimpleBlog;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;


class DefaultController extends Controller
{

    /**
     * Displays a single SimpleBlog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SimpleBlog::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'allCategories' => SimpleBlog::valuesAllCategories(), // берем из модели статик метод который сам и написал
        ]);

    }

    /**
     * Finds the SimpleBlog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SimpleBlog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = SimpleBlog::findOne(['id' => $id]);
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Страница не найдена');
        }
    }
}
