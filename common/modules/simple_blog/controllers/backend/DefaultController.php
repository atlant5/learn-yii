<?php

namespace modules\simple_blog\controllers\backend;

use Yii;
use modules\simple_blog\models\backend\SimpleBlog;
use modules\simple_blog\models\backend\SearchSimpleBlog;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;

/**
 * DefaultController implements the CRUD actions for SimpleBlog model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            [
                'class' => AccessControl::className(),
                // если хотим только для конкретных экшнов применять рбак
                'only' => [
                    'create', 'update', 'delete',
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'create', 'update', 'delete',
                        ],
                        'roles' => [
                            'blog-crud',  // сюда добавляем именно право доступа, можно и конкретно роль, но связь роль-право рулится в другом месте
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all SimpleBlog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchSimpleBlog();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SimpleBlog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SimpleBlog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SimpleBlog();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'dataProvider' => $this->findCategories(), // передаем dataProvider во вьюху
            ]);
        }
    }

    /**
     * Updates an existing SimpleBlog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->categories = $model->loadCategories();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'dataProvider' => $this->findCategories(), // передаем dataProvider во вьюху
            ]);
        }
    }

    /**
     * Deletes an existing SimpleBlog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SimpleBlog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SimpleBlog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SimpleBlog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findCategories(){
        $model = new SimpleBlog();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $model->valuesAllCategories(),
        ]);

        return $dataProvider;
    }

}
