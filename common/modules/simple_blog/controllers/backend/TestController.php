<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 12.02.17
 * Time: 22:52
 */

namespace modules\simple_blog\controllers\backend;
use Yii;


class TestController extends \yii\web\Controller
{
    public function actionIndex(){
        // authtManager - компонент который содержит рбак

        $role = $manager->createPermission('blog-crud');
        $role->description = 'CRUD простого блога';

        $manager->add($role);
    }

    public function actionSaveRole(){
        //
        $manager = Yii::$app->authManager;


        // получаем роль
        $role = $manager->getRole('super-admin');
        $permission = $manager->getPermission('blog-crud');

        $manager->addChild($role, $permission);
    }

    public function actionSaveUserRole(){
        $manager = Yii::$app->authManager;

        $role = $manager->getRole('super-admin');
        $manager->assign($role, 1);
    }

    public function actionTest(){

        return Yii::$app->user->can('blog-crud') ? 'ok' : 'no';
    }
}