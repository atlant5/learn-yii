<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\simple_blog\models\BaseCategories */

$this->title = 'Создание категории';
$this->params['breadcrumbs'][] = ['label' => 'категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageTitle'] = $this->title;
?>
<div class="base-categories-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
