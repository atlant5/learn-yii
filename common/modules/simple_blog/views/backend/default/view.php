<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model modules\simple_blog\models\backend\SimpleBlog */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Simple Blogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageTitle'] = $this->title;

?>
<div class="simple-blog-view  box box-header with-border">
    <?php if (Yii::$app->user->can('blog-crud')): ?>
        <p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php endif; ?>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'own_description',
            'page_title',
            'meta_description',
            'meta_keywords',
            'content:html',
            'publication',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
