<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel modules\simple_blog\models\backend\SearchSimpleBlog */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Простые статьи';
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageTitle'] = $this->title;

?>
<div class="simple-blog-index  box box-header with-border">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if (Yii::$app->user->can('blog-crud')): ?>
        <p>
            <?= Html::a('создать статью', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'id',
                'filter' => false,
            ],
            'page_title',
            // 'meta_keywords',
            // 'content:ntext',
            'publication',
            // 'created_at',
            // 'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => Yii::$app->user->can('blog-crud'),
                    'delete' => Yii::$app->user->can('blog-crud'),
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
