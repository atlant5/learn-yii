<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\simple_blog\models\backend\SimpleBlog */

$this->title = 'Create Simple Blog';
$this->params['breadcrumbs'][] = ['label' => 'Simple Blogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="simple-blog-create">

    <?= $this->render('_form', [
        'model' => $model,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
