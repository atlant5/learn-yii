<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\DatePicker;
use dosamigos\ckeditor\CKEditor;
use froala\froalaeditor\FroalaEditorWidget;

use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $model modules\simple_blog\models\backend\SimpleBlog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="simple-blog-form  box box-header with-border">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'own_description')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'page_title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'publication')->textInput(['value' => date('Y-m-d H:i')])  ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <?= $form->field($model, 'content')->widget('trntv\aceeditor\AceEditor',
        [
            'mode'=>'html', // programing language mode. Default "html"
            'theme'=>'tomorrow_night',
            'containerOptions' => [
                'style' => 'width: 100%; min-height: 300px',
            ]// editor theme. Default "github"
        ])  ?>



    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'categories[]')->hiddenInput() ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'summary' => '',
                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'name' => 'SimpleBlog[categories]',
                        'checkboxOptions' => function ($item_model, $key, $index, $column) use ($model) {
                            return [
                                'checked' => in_array($item_model->id, $model->categories),
                                'value' => $item_model->id,
                            ];
                        }
                        // you may configure additional properties here
                    ],

                    'name',
                ],
            ]) ?>
        </div>
    </div>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'создать' : 'обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
