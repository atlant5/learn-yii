<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
?>

<div class="col-md-6">
    <h2>
        <a href="<?= Url::toRoute(['view', 'id' => $model->id]); ?>"><?= Html::encode($model->title) ?></a>
    </h2>
    <?= HtmlPurifier::process($model->content) ?>
</div>