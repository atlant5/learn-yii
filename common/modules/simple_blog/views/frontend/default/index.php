<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 05.02.17
 * Time: 23:35
 */

use yii\widgets\ListView;
use yii\data\ActiveDataProvider;


?>

<div class="row">
    <div class="col-md-9">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_list',
            'summary' => '',
        ]); ?>
    </div>
    <div class="col-md-3 bg-primary">
        <h3>Категории</h3>
        <ul>

            <?php foreach ($allCategories as $category) {
                print "<li>".$category->name."</li>";
            } ?>
        </ul>
    </div>
</div>


