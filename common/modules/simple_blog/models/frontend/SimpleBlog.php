<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 01.02.17
 * Time: 9:52
 */

namespace modules\simple_blog\models\frontend;

use modules\simple_blog\models\BaseCategories;
use modules\simple_blog\models\BaseSimpleBlog;

class SimpleBlog extends BaseSimpleBlog
{
    public static function valuesAllCategories(){
        return BaseCategories::find()->all();
    }
}