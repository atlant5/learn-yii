<?php

namespace modules\simple_blog\models;

use Yii;

/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property string $title
 * @property string $own_description
 * @property string $page_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $content
 * @property string $publication
 * @property string $created_at
 * @property string $updated_at
 */
class BaseSimpleBlog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['publication', 'created_at', 'updated_at'], 'safe'],
            [['title', 'page_title', 'meta_description', 'meta_keywords'], 'string', 'max' => 255],
            [['own_description'], 'string', 'max' => 800],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '< title >',
            'own_description' => 'Внутреннее примечание',
            'page_title' => 'статья',
            'meta_description' => 'Description',
            'meta_keywords' => 'Ключевики',
            'content' => 'контент',
            'publication' => 'публикация',
            'created_at' => 'создано',
            'updated_at' => 'изменено',
        ];
    }
}
