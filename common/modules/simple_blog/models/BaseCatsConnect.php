<?php

namespace modules\simple_blog\models;

use Yii;

/**
 * This is the model class for table "cats_connect".
 *
 * @property integer $id
 * @property string $post_id
 * @property string $category_id
 * @property string $updated_at
 */
class BaseCatsConnect extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cats_connect';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['updated_at'], 'safe'],
            [['post_id', 'category_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'category_id' => 'Category ID',
            'updated_at' => 'Updated At',
        ];
    }
}
