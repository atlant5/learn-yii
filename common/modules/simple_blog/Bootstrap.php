<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 12.01.17
 * Time: 1:59
 *
 * Бутстрап юзаем для того, что бы заданная таблица роутов подтягивалась сразу.
 * Бутстрап вызывается во время инициализации всего приложения, а не только когда понадобился это модуль
 */

namespace modules\simple_blog;


use modules\simple_blog\components\UrlRule;
use yii\base\BootstrapInterface;
use Yii;

class Bootstrap implements BootstrapInterface {


    public function bootstrap($app) {
        $rules = [
            'simple_blog' => 'simple_blog/default/index',
            'blog/<id:\d+>' => 'simple_blog/default/view',
            'blog' => 'simple_blog/default/index',
        ];

        if (Yii::$app->id == 'app-frontend') {
            $rules[] = [
                'class' => UrlRule::className(),
            ];
        }

        $app->urlManager->addRules($rules, false); // false - добавляет эти правила к существующим в конфиге

        // добавляем информацию в главный конфиг при загрузке, чтоб потом вытащить во вьюхе
        // ужасный метод
        Yii::$app->params['leftMenu'][222] = [
            'label' => '<i class="fa fa-newspaper-o"></i> <span>Простой блог</span>',
            'url' => '#',

            'items' => [
                [
                    'label' => '<i class="fa fa-pencil-square-o"></i> <span>Статьи</span>',
                    'url' => ['/simple_blog/default/index/'],
                ],
                [
                    'label' => '<i class="fa fa-tags"></i> <span>Категории</span>',
                    'url' => ['/simple_blog/category/index'],
                ],
            ]
        ];
    }
}