<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use \modules\rbac\models\RolesForm;


/* @var $this yii\web\View */
/* @var $model modules\general_pages\models\backend\GeneralPages */
/* @var $form yii\widgets\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */



?>

<div class="general-pages-form box box-header with-border">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'alias')->textInput([
                'disabled' => ($model->scenario == RolesForm::SCENARIO_UPDATE),
            ]) ?>
        </div>
        <div class="col-md-9">
            <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton( ($model->scenario == RolesForm::SCENARIO_CREATE ? 'создать разрешение' : 'изменить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>