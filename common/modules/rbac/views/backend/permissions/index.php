<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel modules\pages\models\backend\SearchPages */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Права пользователей';
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageTitle'] = $this->title;

?>
<div class="col-lg-8 col-md-12">
    <div class="col-md-6 pages-index box box-header with-border ">

        <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a('добавить разрешение', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'name',
                    'label' => 'Разрешение'
                ],
                [
                    'attribute' => 'description',
                    'label' => 'Описание'
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                ],
            ],
        ]); ?>
    </div>
</div>
