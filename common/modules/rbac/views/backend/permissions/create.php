<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\general_pages\models\backend\GeneralPages */

$this->title = 'Права (разрешения) пользователей';
$this->params['breadcrumbs'][] = ['label' => 'Права', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'создать';

$this->params['pageTitle'] = $this->title;
?>

<div class="col-lg-8">
    <div class="general-pages-create">

        <?= $this->render('_form', [
            'model' => $model
        ]) ?>

    </div>
</div>
