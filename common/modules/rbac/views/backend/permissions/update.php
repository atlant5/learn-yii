<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\general_pages\models\backend\GeneralPages */

$this->title = 'Права пользователей';
$this->params['breadcrumbs'][] = ['label' => 'Роли', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->alias;

$this->params['pageTitle'] = $this->title;
?>

<div class="col-lg-8">
    <div class="general-pages-create">

        <?= $this->render('_form', [
            'model' => $model
        ]) ?>

    </div>
</div>
