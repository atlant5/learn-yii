<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\general_pages\models\backend\GeneralPages */

$this->title = 'Роли пользователей';
$this->params['breadcrumbs'][] = ['label' => 'Роли', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'создать';

$this->params['pageTitle'] = $this->title;
?>

<div class="col-lg-8">
    <div class="general-pages-create">

        <?= $this->render('_form', [
            'model' => $model
        ]) ?>

    </div>
</div>
