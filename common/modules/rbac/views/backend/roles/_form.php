<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use \modules\rbac\models\RolesForm;


/* @var $this yii\web\View */
/* @var $model modules\general_pages\models\backend\GeneralPages */
/* @var $form yii\widgets\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */



?>

<div class="general-pages-form box box-header with-border">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'alias')->textInput([
                'disabled' => ($model->scenario == RolesForm::SCENARIO_UPDATE),
            ]) ?>
        </div>
        <div class="col-md-9">
            <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 pull-right">
            <br>
            <?= $form->field($model, 'permissions[]')->hiddenInput() ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => '{items} {pager}',
                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'name' => 'RolesForm[permissions]', //модель и свойство
                        'checkboxOptions' => function ($item_model, $key, $index, $column) use ($model) {
                            return [
                                'checked' => in_array($item_model->name, $model->permissions),
                            ];
                        }
                        // you may configure additional properties here
                    ],
                    [
                        'attribute' => 'name',
                        'label' => 'Разрешение'
                    ],
                    [
                        'attribute' => 'description',
                        'label' => 'Описание'
                    ],
                ],
            ]) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton( ($model->scenario == RolesForm::SCENARIO_CREATE ? 'создать роль' : 'изменить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>