<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 12.02.17
 * Time: 23:57
 */


namespace modules\rbac\models;

use yii\base\Model;
use Yii;
use yii\helpers\VarDumper;

// этот класс мы не связываем с бд, тут связь с файлами
class PermissionsForm extends Model
{
    //
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';



    /**
     * Название разрешения
     * @var
     */
    public $alias;
    /**
     * Описание разрешения в нескольких словах / фразах
     * @var
     */
    public $description;



    public function rules()
    {
        return [
            [['alias', 'description'], 'required', ],
            [['description'], 'string', 'max' => 255,],
            [['alias'], 'string', 'max' => 63,],
            ['alias', 'validateAlias',],
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => [
                'alias', 'description'
            ],
            self::SCENARIO_UPDATE => [
                'description',
            ]
        ];
    }

    public function validateAlias($attribute, $params = []){
        // узнаем есть роль или нет
        $manager = Yii::$app->authManager;
        $role = $manager->getRole($this->alias);

        if ($role !== null) {
            $this->addError('alias', 'Такое разрешение уже есть');
        }

    }

    public function attributeLabels(){
        return [
            'alias' => "Разрешение",
            'description' => "Описание",
        ];
    }



    // учим сохранять разрешение
    public function save(){
        // если валидация не пройдена, то ничего не сохраняем, возвращаем false
        if ($this->validate() === false){
            return false;
        }

        $manager = Yii::$app->authManager;
        $permission = $manager->createPermission($this->alias);
        $permission->description = $this->description;

        return $manager->add($permission); // сохраняем
    }


    public function findPermission($alias){
        $manager = Yii::$app->authManager;
        $permission = $manager->getPermission($alias);

        if ($permission === null) {
            return false;
        }


        $this->alias = $permission->name;
        $this->description = $permission->description;
        return true;
    }

    public function delete(){
        $manager = Yii::$app->authManager;
        $permission = $manager->getPermission($this->alias);
        return $manager->remove($permission);
    }


}