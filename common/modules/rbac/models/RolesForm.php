<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 12.02.17
 * Time: 23:57
 */


namespace modules\rbac\models;

use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

// этот класс мы не связываем с бд, тут связь с файлами
class RolesForm extends Model
{
    //
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';



    /**
     * Название роли
     * @var
     */
    public $alias;
    /**
     * Описание роли в нескольких словах / фразах
     * @var
     */
    public $description;

    /**
     * Переменная содержащая список прав доступа для конкретной роли (alias-а)
     * @var array
     */
    public $permissions = [];

    public function rules()
    {
        return [
            [['alias', 'description'], 'required', ],
            [['description'], 'string', 'max' => 255,],
            [['alias'], 'string', 'max' => 63,],
            ['alias', 'validateAlias',],
            [['permissions'], 'safe'] // добавляем разрешение на редактирование и передачу полей с таким именем
        ];
    }

    /**
     * в сценарии указывается поля которые принимать от формы
     * причем разделяем обработку полей
     * чтоб создавать и редактировать красивей
     * @return array
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => [
                'alias', 'description','permissions'
            ],
            self::SCENARIO_UPDATE => [
                'description', 'permissions'
            ]
        ];
    }

    public function validateAlias($attribute, $params = []){
        // узнаем есть роль или нет
        $manager = Yii::$app->authManager;
        $role = $manager->getRole($this->alias);

        if ($role !== null) {
            $this->addError('alias', 'Такая роль уже есть');
        }

    }

    public function attributeLabels(){
        return [
            'alias' => "Роль",
            'description' => "Описание",
            'permissions' => "Права доступа"
        ];
    }



    // учим сохранять роль
    public function save(){
        // если валидация не пройдена, то ничего не сохраняем, возвращаем false
        if ($this->validate() === false){
            return false;
        }

        $manager = Yii::$app->authManager;
        $role = $manager->createRole($this->alias);
        $role->description = $this->description;


        $manager->add($role); // сохраняем
        $this->savePermissions();
        return true;
    }

    protected function  savePermissions(){

        // берём имеющиеся в классе праава из массива для этого предназначенного
        if (is_array($this->permissions) === false) {
            $this->permissions = [];
        }

        // объявляем менеджер
        $manager = Yii::$app->authManager;

        $role  = $manager->getRole($this->alias); // получаем объект роли, которая хранится в alias
        $manager->removeChildren($role); // удаляет из items.php все дочерние роли/права доступа только для роли из alias

        // на момент сохранения в $this->permissions
        // передаются permissions из отмеченных галками
        // в табличке во вьюхе
        foreach ($this->permissions as $permission_alias) {
            // в  $permission передаем экземпляр объекта
            $permission = $manager->getPermission($permission_alias);
            // если объект существует, добавляем в качестве ребенка - право (записываем в файл items.php)
            if ($permission) {
                $manager->addChild($role, $permission);
            }
        }
    }


    public function loadPermissions(){
        // объявляем менеджер
        $manager = Yii::$app->authManager;

        return ArrayHelper::map($manager->getPermissionsByRole($this->alias), 'name', 'name');
    }


    public function findRole($alias){
        $manager = Yii::$app->authManager;
        $role = $manager->getRole($alias);

        if ($role === null) {
            return false;
        }

        $this->alias = $role->name;
        $this->description = $role->description;
        return true;
    }

    /**
     * @return bool
     */
    public function delete(){
        $manager = Yii::$app->authManager;
        $role = $manager->getRole($this->alias);
        return $manager->remove($role);
    }

    /**
     * Возвращаем массив прав для того что бы потом их передать во вьюху редактирования
     * ролей и назначать конкретной роли
     * @return \yii\rbac\Permission[]
     */
    public function allPermissions(){
        $manager = Yii::$app->authManager;
        return $manager->getPermissions();
    }


}