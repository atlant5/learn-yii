<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 13.02.17
 * Time: 0:13
 */

namespace modules\rbac\controllers\backend;


use modules\rbac\models\PermissionsForm;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

class PermissionsController extends Controller
{


    public function actionIndex(){
        //
        $allPermissions = Yii::$app->authManager->getPermissions();

        $dataProvider = new ArrayDataProvider([
            'models' => $allPermissions,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate(){
        $model = new PermissionsForm();
        $model->scenario = PermissionsForm::SCENARIO_CREATE;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id){
        $model = new PermissionsForm();
        $model->scenario = PermissionsForm::SCENARIO_UPDATE;

        if ($model->findPermission($id) == false){
            throw new NotFoundHttpException('Страница не найдена');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);

    }

    public function actionDelete($id){
        $model = new PermissionsForm();
        if ($model->findPermission($id) == false){
            throw new NotFoundHttpException('Страница не найдена');
        }
        if ($model->delete() == false) {
            throw new NotFoundHttpException('Ошибка удаления');
        }
        return $this->redirect(['index']);
    }
}