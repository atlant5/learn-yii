<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 13.02.17
 * Time: 0:13
 */

namespace modules\rbac\controllers\backend;


use modules\rbac\models\RolesForm;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

class RolesController extends Controller
{


    public function actionIndex(){
        //
        $allRoles = Yii::$app->authManager->getRoles();

        $dataProvider = new ArrayDataProvider([
            'models' => $allRoles,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate(){
        $model = new RolesForm();
        $model->scenario = RolesForm::SCENARIO_CREATE;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id){
        $model = new RolesForm();
        $model->scenario = RolesForm::SCENARIO_UPDATE;

        if ($model->findRole($id) == false){
            throw new NotFoundHttpException('Страница не найдена');
        }

        $model->permissions = $model->loadPermissions();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Данные успешно сохранены!');
            return $this->redirect(['index']);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $model->allPermissions(),
        ]);

        return $this->render('update', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionDelete($id){
        $model = new RolesForm();
        if ($model->findRole($id) == false){
            throw new NotFoundHttpException('Страница не найдена');
        }
        if ($model->delete() == false) {
            throw new NotFoundHttpException('Ошибка удаления');
        }
        return $this->redirect(['index']);
    }
}