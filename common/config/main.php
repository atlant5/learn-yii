<?php
return [
    'language' => 'ru',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
        ],
        'formatter' => [
            'class' => '\yii\i18n\Formatter',
            'dateFormat' => 'php:d M Y',
            'datetimeFormat' => 'php:d.m.Y | H:i:s',
            'timeFormat' => 'php:H:i:s',
        ],
    ],
];
