<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@modules', dirname(__DIR__) . '/modules');

// dirname(dirname(__DIR__)) <- ВОТ ТАК НАДО БЫЛО!!! а то 404 вечная и не ищит контроллер в /api
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api');   // api приложение, его конфиги. Это API V1,
                                                    // другие версии API будут в других папках, главное понять
                                                    // геморойность и необходимость поддержки ранних версий API в разных папках
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
