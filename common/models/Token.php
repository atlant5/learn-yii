<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 04.04.17
 * Time: 22:35
 */

namespace common\models;


use yii\db\ActiveRecord;

/**
 * Class Token
 * @package common\models
 *
 * @property integer $expired_at
 * @property string $token
 */
class Token extends ActiveRecord
{
    public static function tableName()
    {
        return 'token';
    }

    // передаем дату истечения токена в $expire
    public function generateToken($expire){
        $this->expired_at = $expire;
        $this->token = \Yii::$app->security->generateRandomString();
    }

    public function fields()
    {
        return [
            'token' => 'token',
            'expired' => function () {
                return date(DATE_RFC3339, $this->expired_at);
            },
        ];
    }
}